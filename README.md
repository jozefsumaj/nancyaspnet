# README #

This is intended to run on dotnet-dev-1.0.0-preview2-1-003177

### What is this repository for? ###

* Jozef use's this to kick-off his dotnet core api projects

### How do I get set up? ###

* Install dotnet
* Clone the repo
* Run dotnet restore
* Run dotnet run

### Who do I talk to? ###

* jozef.sumaj@gmail.com